#!/bin/bash
uvcdynctrl -s 'LED1 Mode' 0
mkdir -p $(date '+%Y-%m-%d')
fswebcam --no-banner -d /dev/video1 -r 1920x1080 --jpeg 95 -F 5 $(date '+%Y-%m-%d')/$(date '+%Y-%m-%d_%H%M%S').jpg

cd $(date -d '1 days ago' '+%Y-%m-%d')
LENGTH=`ls | wc -l | wc -L`
NUMBER="1"
find *.jpg | while read foto
do
  mv $foto "`printf %0${LENGTH}d $NUMBER`".jpg
      NUMBER=$((NUMBER + 1))
          done
          ffmpeg -f image2 -i %0${LENGTH}d.jpg -r 25 -vcodec libx264 ../foobar.mpg
          mv foobar.mpg $(date -d '1 days ago' '+%Y-%m-%d').mpg
          rm *.jpg
          cd /home/elektroll/Bilder/wc1/
          rm -R $(date -d '1 days ago' '+%Y-%m-%d')
          youtube-upload --title "Time lapse Sky - $(date -d '1 days ago' '+%Y-%m-%d')" --description="Time lapse video out of my kitchen windows" --tags="window, Time lapse, zeitraffer, Wetzlar" --privacy="private" --publish-at="$(date -d '+1 day' '+%Y-%m-%d')T16:00:00.000+02:00" $(date -d '1 days ago' '+%Y-%m-%d').mpg
          rm $(date -d '1 days ago' '+%Y-%m-%d').mpg
          echo $(pwd)
